from sqlalchemy.types import TypeDecorator, SMALLINT
from typing import Optional

from src.app.value_object.age_value_object import Age


class AgeDecorator(TypeDecorator):
    impl = SMALLINT

    def process_bind_param(self, value_object: Age, dialect) -> Optional[int]:
        return None if value_object is None else value_object.get_value()

    def process_result_value(self, value: Optional[int], dialect) -> Optional[Age]:
        return None if value is None else Age(value)
