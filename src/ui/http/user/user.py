from fastapi import APIRouter, HTTPException
from src.user.repository.user_repository import UserRepository
from src.app.value_object.id_value_object import Id
from src.app.value_object.name_value_object import Name
from src.app.value_object.age_value_object import Age
from src.app.value_object.description_value_object import Description
from src.user.model.user.user import User
from pydantic import BaseModel
from typing import Optional

router = APIRouter()
userRepository = UserRepository()


class UserDto(BaseModel):
    name: str
    description: Optional[str] = None
    age: Optional[int] = None


class UserUpdate(UserDto):
    name: Optional[str]


@router.post("/user")
def create(userDto: UserDto):
    user = userRepository.add(
        User(
            Name(userDto.name),
            None if userDto.description is None else Description(userDto.description),
            None if userDto.age is None else Age(userDto.age),
        )
    )

    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value,
        'age':  None if user.age is None else user.age.value,
    }


@router.get("/user")
def index() -> list:
    users = userRepository.list()
    return list(
        map(
            lambda user:
            {
                'id': user.id.value,
                'name': user.name.value,
                'description': None if user.description is None else user.description.value,
                'age': None if user.age is None else user.age.value,
            }, users
        )
    )


@router.get("/user/{id}")
def view(id: int):
    user = userRepository.get(Id(id))
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value,
        'age': None if user.age is None else user.age.value,
    }


@router.patch("/user/{id}")
def update(id: int, userUpdate: UserUpdate):
    user = userRepository.get(Id(id))
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')

    if userUpdate.name is not None:
        user.name = Name(userUpdate.name)
    if userUpdate.age is not None:
        user.age = Age(userUpdate.age)
    if userUpdate.description is not None:
        user.description = Description(userUpdate.description)
    userRepository.update()

    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value,
        'age': None if user.age is None else user.age.value,
    }


@router.delete("/user/{id}")
def delete(id: int):
    user = userRepository.get(Id(id))
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    userRepository.delete(user)
    return {'message': f'Deleted user {id}'}
