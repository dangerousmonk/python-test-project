"""add age field

Revision ID: 8a766dd8ca99
Revises: 2f4537358ee6
Create Date: 2021-09-05 21:37:32.706170

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8a766dd8ca99'
down_revision = '2f4537358ee6'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'user',
        sa.Column('age', sa.SmallInteger, nullable=True)
    )


def downgrade():
    op.drop_column('user', 'age')
